# Repositório do controlador de posição dos robôs da SSL

Neste repositório você vai encontrar:
* Os dados utilizados para estimar o(s) modelo(s) dos robôs;
* O modelo no Simulink do controlador PID utilizado para posicionar os robôs em campo;
* O código em C++ do controlador atualmente utilizado.

# Como usar?

## Identificar os modelos do robô

Caso seja necessário identificar os modelos do robô devido à mudanças na
mecânica ou no firmware, o jeito mais fácil é utilizar a ferramenta para
aquisição e identificação de modelos. Ela pode ser encontrada [neste
repositório](https://gitlab.com/leo_costa/systemidentificationsoftware).

Essa ferramenta possui uma interface gráfica para execução dos testes
necessários, assim como a identificação dos modelos do tipo ARX, ARMAX e Função
de Transferência.

Caso seja necessário utilizar esses modelos dentro do MATLAB, é mais prático
utilizar a ferramenta para coletar os dados e então usar os scripts contidos
neste repositório.  

Existem dois scripts que podem ser utilizados, um para identificar as funções
de transferência, e o outro para identificar os modelos polinomiais ARX e
ARMAX. Ambos funcionam da mesma forma, ao rodar o script aparecerá um prompt 
perguntando o local da pasta onde estão os dados, basta indicar a pasta que 
possui os arquivos 'Data_X.csv', 'Data_Y.csv' e  'Data_W.csv'.

Neste repositório existem dois conjuntos de dados:
* Para o modelo de FT: [Dados](..master/modelData/STEP_FT)
* Para os modelos ARX e ARMAX: [Dados](..master/modelData/GBN_ARX_ARMAX)

## Modificar/atualizar o controlador PID

Para modificar o código do controlador PID basta fazer as alterações no Simulink
e recompilar os arquivos em C++.

Neste [repositório](https://gitlab.com/leo_costa/ic_identificacao) existe um tutorial explicando o passa a passo para fazer 
isso.
