/*
 * ControlePosicao.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 2.0
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Wed Jun 16 13:00:34 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_h_
#define RTW_HEADER_ControlePosicao_h_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "ControlePosicao_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S89>/Integrator' */
  real_T Filter_DSTATE;                /* '<S84>/Filter' */
  real_T TFPosicaoX_states[2];         /* '<Root>/TFPosicaoX' */
  real_T Integrator_DSTATE_i;          /* '<S139>/Integrator' */
  real_T Filter_DSTATE_b;              /* '<S134>/Filter' */
  real_T TFPosicaoY_states[2];         /* '<Root>/TFPosicaoY' */
  real_T Integrator_DSTATE_b;          /* '<S39>/Integrator' */
  real_T Filter_DSTATE_d;              /* '<S34>/Filter' */
  real_T TFPosicaoAngular_states[2];   /* '<Root>/TFPosicaoAngular' */
  int8_T Integrator_PrevResetState;    /* '<S89>/Integrator' */
  int8_T Filter_PrevResetState;        /* '<S84>/Filter' */
  int8_T Integrator_PrevResetState_p;  /* '<S139>/Integrator' */
  int8_T Filter_PrevResetState_o;      /* '<S134>/Filter' */
} DW_ControlePosicao_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T PosX;                         /* '<Root>/PosX' */
  real_T PosY;                         /* '<Root>/PosY' */
  real_T ReAlimX;                      /* '<Root>/ReAlimX' */
  real_T ReAlimY;                      /* '<Root>/ReAlimY' */
  real_T PosAngular;                   /* '<Root>/PosAngular' */
  real_T ReAlimAngular;                /* '<Root>/ReAlimAngular' */
} ExtU_ControlePosicao_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T SinalControleX;               /* '<Root>/SinalControleX' */
  real_T SinalControleY;               /* '<Root>/SinalControleY' */
  real_T PosRoboX;                     /* '<Root>/PosRoboX' */
  real_T PosRoboY;                     /* '<Root>/PosRoboY' */
  real_T ErroX;                        /* '<Root>/ErroX' */
  real_T ErroY;                        /* '<Root>/ErroY' */
  real_T SinalControleW;               /* '<Root>/SinalControleW' */
  real_T PosRoboAngular;               /* '<Root>/PosRoboAngular' */
  real_T ErroW;                        /* '<Root>/ErroW' */
} ExtY_ControlePosicao_T;

/* Real-time Model Data Structure */
struct tag_RTM_ControlePosicao_T {
  const char_T *errorStatus;
};

/* Class declaration for model ControlePosicao */
class ControlePosicaoModelClass {
  /* public data and function members */
 public:
  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  ControlePosicaoModelClass();

  /* Destructor */
  ~ControlePosicaoModelClass();

  /* Root-level structure-based inputs set method */

  /* Root inports set method */
  void setExternalInputs(const ExtU_ControlePosicao_T* pExtU_ControlePosicao_T)
  {
    ControlePosicao_U = *pExtU_ControlePosicao_T;
  }

  /* Root-level structure-based outputs get method */

  /* Root outports get method */
  const ExtY_ControlePosicao_T & getExternalOutputs() const
  {
    return ControlePosicao_Y;
  }

  /* Real-Time Model get method */
  RT_MODEL_ControlePosicao_T * getRTM();

  /* private data and function members */
 private:
  /* Block states */
  DW_ControlePosicao_T ControlePosicao_DW;

  /* External inputs */
  ExtU_ControlePosicao_T ControlePosicao_U;

  /* External outputs */
  ExtY_ControlePosicao_T ControlePosicao_Y;

  /* Real-Time Model */
  RT_MODEL_ControlePosicao_T ControlePosicao_M;

  /* private member function(s) for subsystem '<S4>/If Action Subsystem'*/
  static void ControlePosic_IfActionSubsystem(real_T rtu_In1, real_T *rty_Out1);
};

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Scope' : Unused code path elimination
 * Block '<Root>/Scope1' : Unused code path elimination
 * Block '<Root>/Scope2' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ControlePosicao'
 * '<S1>'   : 'ControlePosicao/ControladorAngular'
 * '<S2>'   : 'ControlePosicao/ControladorX'
 * '<S3>'   : 'ControlePosicao/ControladorY'
 * '<S4>'   : 'ControlePosicao/PID_Reset'
 * '<S5>'   : 'ControlePosicao/PID_Reset1'
 * '<S6>'   : 'ControlePosicao/ControladorAngular/Anti-windup'
 * '<S7>'   : 'ControlePosicao/ControladorAngular/D Gain'
 * '<S8>'   : 'ControlePosicao/ControladorAngular/Filter'
 * '<S9>'   : 'ControlePosicao/ControladorAngular/Filter ICs'
 * '<S10>'  : 'ControlePosicao/ControladorAngular/I Gain'
 * '<S11>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain'
 * '<S12>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain Fdbk'
 * '<S13>'  : 'ControlePosicao/ControladorAngular/Integrator'
 * '<S14>'  : 'ControlePosicao/ControladorAngular/Integrator ICs'
 * '<S15>'  : 'ControlePosicao/ControladorAngular/N Copy'
 * '<S16>'  : 'ControlePosicao/ControladorAngular/N Gain'
 * '<S17>'  : 'ControlePosicao/ControladorAngular/P Copy'
 * '<S18>'  : 'ControlePosicao/ControladorAngular/Parallel P Gain'
 * '<S19>'  : 'ControlePosicao/ControladorAngular/Reset Signal'
 * '<S20>'  : 'ControlePosicao/ControladorAngular/Saturation'
 * '<S21>'  : 'ControlePosicao/ControladorAngular/Saturation Fdbk'
 * '<S22>'  : 'ControlePosicao/ControladorAngular/Sum'
 * '<S23>'  : 'ControlePosicao/ControladorAngular/Sum Fdbk'
 * '<S24>'  : 'ControlePosicao/ControladorAngular/Tracking Mode'
 * '<S25>'  : 'ControlePosicao/ControladorAngular/Tracking Mode Sum'
 * '<S26>'  : 'ControlePosicao/ControladorAngular/Tsamp - Integral'
 * '<S27>'  : 'ControlePosicao/ControladorAngular/Tsamp - Ngain'
 * '<S28>'  : 'ControlePosicao/ControladorAngular/postSat Signal'
 * '<S29>'  : 'ControlePosicao/ControladorAngular/preSat Signal'
 * '<S30>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping Parallel'
 * '<S31>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping Parallel/Dead Zone'
 * '<S32>'  : 'ControlePosicao/ControladorAngular/Anti-windup/Disc. Clamping Parallel/Dead Zone/Enabled'
 * '<S33>'  : 'ControlePosicao/ControladorAngular/D Gain/Internal Parameters'
 * '<S34>'  : 'ControlePosicao/ControladorAngular/Filter/Disc. Forward Euler Filter'
 * '<S35>'  : 'ControlePosicao/ControladorAngular/Filter ICs/Internal IC - Filter'
 * '<S36>'  : 'ControlePosicao/ControladorAngular/I Gain/Internal Parameters'
 * '<S37>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain/Passthrough'
 * '<S38>'  : 'ControlePosicao/ControladorAngular/Ideal P Gain Fdbk/Disabled'
 * '<S39>'  : 'ControlePosicao/ControladorAngular/Integrator/Discrete'
 * '<S40>'  : 'ControlePosicao/ControladorAngular/Integrator ICs/Internal IC'
 * '<S41>'  : 'ControlePosicao/ControladorAngular/N Copy/Disabled'
 * '<S42>'  : 'ControlePosicao/ControladorAngular/N Gain/Internal Parameters'
 * '<S43>'  : 'ControlePosicao/ControladorAngular/P Copy/Disabled'
 * '<S44>'  : 'ControlePosicao/ControladorAngular/Parallel P Gain/Internal Parameters'
 * '<S45>'  : 'ControlePosicao/ControladorAngular/Reset Signal/Disabled'
 * '<S46>'  : 'ControlePosicao/ControladorAngular/Saturation/Enabled'
 * '<S47>'  : 'ControlePosicao/ControladorAngular/Saturation Fdbk/Disabled'
 * '<S48>'  : 'ControlePosicao/ControladorAngular/Sum/Sum_PID'
 * '<S49>'  : 'ControlePosicao/ControladorAngular/Sum Fdbk/Disabled'
 * '<S50>'  : 'ControlePosicao/ControladorAngular/Tracking Mode/Disabled'
 * '<S51>'  : 'ControlePosicao/ControladorAngular/Tracking Mode Sum/Passthrough'
 * '<S52>'  : 'ControlePosicao/ControladorAngular/Tsamp - Integral/Passthrough'
 * '<S53>'  : 'ControlePosicao/ControladorAngular/Tsamp - Ngain/Passthrough'
 * '<S54>'  : 'ControlePosicao/ControladorAngular/postSat Signal/Forward_Path'
 * '<S55>'  : 'ControlePosicao/ControladorAngular/preSat Signal/Forward_Path'
 * '<S56>'  : 'ControlePosicao/ControladorX/Anti-windup'
 * '<S57>'  : 'ControlePosicao/ControladorX/D Gain'
 * '<S58>'  : 'ControlePosicao/ControladorX/Filter'
 * '<S59>'  : 'ControlePosicao/ControladorX/Filter ICs'
 * '<S60>'  : 'ControlePosicao/ControladorX/I Gain'
 * '<S61>'  : 'ControlePosicao/ControladorX/Ideal P Gain'
 * '<S62>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk'
 * '<S63>'  : 'ControlePosicao/ControladorX/Integrator'
 * '<S64>'  : 'ControlePosicao/ControladorX/Integrator ICs'
 * '<S65>'  : 'ControlePosicao/ControladorX/N Copy'
 * '<S66>'  : 'ControlePosicao/ControladorX/N Gain'
 * '<S67>'  : 'ControlePosicao/ControladorX/P Copy'
 * '<S68>'  : 'ControlePosicao/ControladorX/Parallel P Gain'
 * '<S69>'  : 'ControlePosicao/ControladorX/Reset Signal'
 * '<S70>'  : 'ControlePosicao/ControladorX/Saturation'
 * '<S71>'  : 'ControlePosicao/ControladorX/Saturation Fdbk'
 * '<S72>'  : 'ControlePosicao/ControladorX/Sum'
 * '<S73>'  : 'ControlePosicao/ControladorX/Sum Fdbk'
 * '<S74>'  : 'ControlePosicao/ControladorX/Tracking Mode'
 * '<S75>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum'
 * '<S76>'  : 'ControlePosicao/ControladorX/Tsamp - Integral'
 * '<S77>'  : 'ControlePosicao/ControladorX/Tsamp - Ngain'
 * '<S78>'  : 'ControlePosicao/ControladorX/postSat Signal'
 * '<S79>'  : 'ControlePosicao/ControladorX/preSat Signal'
 * '<S80>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping Parallel'
 * '<S81>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping Parallel/Dead Zone'
 * '<S82>'  : 'ControlePosicao/ControladorX/Anti-windup/Disc. Clamping Parallel/Dead Zone/Enabled'
 * '<S83>'  : 'ControlePosicao/ControladorX/D Gain/Internal Parameters'
 * '<S84>'  : 'ControlePosicao/ControladorX/Filter/Disc. Forward Euler Filter'
 * '<S85>'  : 'ControlePosicao/ControladorX/Filter ICs/Internal IC - Filter'
 * '<S86>'  : 'ControlePosicao/ControladorX/I Gain/Internal Parameters'
 * '<S87>'  : 'ControlePosicao/ControladorX/Ideal P Gain/Passthrough'
 * '<S88>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk/Disabled'
 * '<S89>'  : 'ControlePosicao/ControladorX/Integrator/Discrete'
 * '<S90>'  : 'ControlePosicao/ControladorX/Integrator ICs/Internal IC'
 * '<S91>'  : 'ControlePosicao/ControladorX/N Copy/Disabled'
 * '<S92>'  : 'ControlePosicao/ControladorX/N Gain/Internal Parameters'
 * '<S93>'  : 'ControlePosicao/ControladorX/P Copy/Disabled'
 * '<S94>'  : 'ControlePosicao/ControladorX/Parallel P Gain/Internal Parameters'
 * '<S95>'  : 'ControlePosicao/ControladorX/Reset Signal/External Reset'
 * '<S96>'  : 'ControlePosicao/ControladorX/Saturation/Enabled'
 * '<S97>'  : 'ControlePosicao/ControladorX/Saturation Fdbk/Disabled'
 * '<S98>'  : 'ControlePosicao/ControladorX/Sum/Sum_PID'
 * '<S99>'  : 'ControlePosicao/ControladorX/Sum Fdbk/Disabled'
 * '<S100>' : 'ControlePosicao/ControladorX/Tracking Mode/Disabled'
 * '<S101>' : 'ControlePosicao/ControladorX/Tracking Mode Sum/Passthrough'
 * '<S102>' : 'ControlePosicao/ControladorX/Tsamp - Integral/Passthrough'
 * '<S103>' : 'ControlePosicao/ControladorX/Tsamp - Ngain/Passthrough'
 * '<S104>' : 'ControlePosicao/ControladorX/postSat Signal/Forward_Path'
 * '<S105>' : 'ControlePosicao/ControladorX/preSat Signal/Forward_Path'
 * '<S106>' : 'ControlePosicao/ControladorY/Anti-windup'
 * '<S107>' : 'ControlePosicao/ControladorY/D Gain'
 * '<S108>' : 'ControlePosicao/ControladorY/Filter'
 * '<S109>' : 'ControlePosicao/ControladorY/Filter ICs'
 * '<S110>' : 'ControlePosicao/ControladorY/I Gain'
 * '<S111>' : 'ControlePosicao/ControladorY/Ideal P Gain'
 * '<S112>' : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk'
 * '<S113>' : 'ControlePosicao/ControladorY/Integrator'
 * '<S114>' : 'ControlePosicao/ControladorY/Integrator ICs'
 * '<S115>' : 'ControlePosicao/ControladorY/N Copy'
 * '<S116>' : 'ControlePosicao/ControladorY/N Gain'
 * '<S117>' : 'ControlePosicao/ControladorY/P Copy'
 * '<S118>' : 'ControlePosicao/ControladorY/Parallel P Gain'
 * '<S119>' : 'ControlePosicao/ControladorY/Reset Signal'
 * '<S120>' : 'ControlePosicao/ControladorY/Saturation'
 * '<S121>' : 'ControlePosicao/ControladorY/Saturation Fdbk'
 * '<S122>' : 'ControlePosicao/ControladorY/Sum'
 * '<S123>' : 'ControlePosicao/ControladorY/Sum Fdbk'
 * '<S124>' : 'ControlePosicao/ControladorY/Tracking Mode'
 * '<S125>' : 'ControlePosicao/ControladorY/Tracking Mode Sum'
 * '<S126>' : 'ControlePosicao/ControladorY/Tsamp - Integral'
 * '<S127>' : 'ControlePosicao/ControladorY/Tsamp - Ngain'
 * '<S128>' : 'ControlePosicao/ControladorY/postSat Signal'
 * '<S129>' : 'ControlePosicao/ControladorY/preSat Signal'
 * '<S130>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping Parallel'
 * '<S131>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping Parallel/Dead Zone'
 * '<S132>' : 'ControlePosicao/ControladorY/Anti-windup/Disc. Clamping Parallel/Dead Zone/Enabled'
 * '<S133>' : 'ControlePosicao/ControladorY/D Gain/Internal Parameters'
 * '<S134>' : 'ControlePosicao/ControladorY/Filter/Disc. Forward Euler Filter'
 * '<S135>' : 'ControlePosicao/ControladorY/Filter ICs/Internal IC - Filter'
 * '<S136>' : 'ControlePosicao/ControladorY/I Gain/Internal Parameters'
 * '<S137>' : 'ControlePosicao/ControladorY/Ideal P Gain/Passthrough'
 * '<S138>' : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk/Disabled'
 * '<S139>' : 'ControlePosicao/ControladorY/Integrator/Discrete'
 * '<S140>' : 'ControlePosicao/ControladorY/Integrator ICs/Internal IC'
 * '<S141>' : 'ControlePosicao/ControladorY/N Copy/Disabled'
 * '<S142>' : 'ControlePosicao/ControladorY/N Gain/Internal Parameters'
 * '<S143>' : 'ControlePosicao/ControladorY/P Copy/Disabled'
 * '<S144>' : 'ControlePosicao/ControladorY/Parallel P Gain/Internal Parameters'
 * '<S145>' : 'ControlePosicao/ControladorY/Reset Signal/External Reset'
 * '<S146>' : 'ControlePosicao/ControladorY/Saturation/Enabled'
 * '<S147>' : 'ControlePosicao/ControladorY/Saturation Fdbk/Disabled'
 * '<S148>' : 'ControlePosicao/ControladorY/Sum/Sum_PID'
 * '<S149>' : 'ControlePosicao/ControladorY/Sum Fdbk/Disabled'
 * '<S150>' : 'ControlePosicao/ControladorY/Tracking Mode/Disabled'
 * '<S151>' : 'ControlePosicao/ControladorY/Tracking Mode Sum/Passthrough'
 * '<S152>' : 'ControlePosicao/ControladorY/Tsamp - Integral/Passthrough'
 * '<S153>' : 'ControlePosicao/ControladorY/Tsamp - Ngain/Passthrough'
 * '<S154>' : 'ControlePosicao/ControladorY/postSat Signal/Forward_Path'
 * '<S155>' : 'ControlePosicao/ControladorY/preSat Signal/Forward_Path'
 * '<S156>' : 'ControlePosicao/PID_Reset/If Action Subsystem'
 * '<S157>' : 'ControlePosicao/PID_Reset/If Action Subsystem1'
 * '<S158>' : 'ControlePosicao/PID_Reset1/If Action Subsystem'
 * '<S159>' : 'ControlePosicao/PID_Reset1/If Action Subsystem1'
 */
#endif                                 /* RTW_HEADER_ControlePosicao_h_ */
