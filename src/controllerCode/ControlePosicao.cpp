/*
 * ControlePosicao.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 2.0
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Wed Jun 16 13:00:34 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#include "ControlePosicao.h"
#include "ControlePosicao_private.h"

/*
 * Output and update for action system:
 *    '<S4>/If Action Subsystem'
 *    '<S4>/If Action Subsystem1'
 *    '<S5>/If Action Subsystem'
 *    '<S5>/If Action Subsystem1'
 */
void ControlePosicaoModelClass::ControlePosic_IfActionSubsystem(real_T rtu_In1,
  real_T *rty_Out1)
{
  /* Inport: '<S156>/In1' */
  *rty_Out1 = rtu_In1;
}

/* Model step function */
void ControlePosicaoModelClass::step()
{
  real_T denAccum;
  real_T denAccum_0;
  real_T denAccum_1;
  real_T rtb_FilterCoefficient;
  real_T rtb_FilterCoefficient_f;
  real_T rtb_FilterCoefficient_l;
  real_T rtb_IntegralGain;
  real_T rtb_Integrator_d;
  real_T rtb_Integrator_p;
  real_T rtb_Merge;
  real_T rtb_Merge_j;
  real_T rtb_SignPreIntegrator;
  boolean_T rtb_Equal1;
  boolean_T rtb_Equal1_lz;
  boolean_T rtb_NotEqual;
  boolean_T rtb_NotEqual_o;
  boolean_T rtb_NotEqual_p2;

  /* Sum: '<Root>/Sum2' incorporates:
   *  Inport: '<Root>/PosX'
   *  Inport: '<Root>/ReAlimX'
   */
  rtb_Integrator_p = ControlePosicao_U.PosX - ControlePosicao_U.ReAlimX;

  /* If: '<S4>/If' incorporates:
   *  Constant: '<S4>/Constant'
   *  Constant: '<S4>/Constant1'
   */
  if (rtb_Integrator_p <= 0.001) {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem' incorporates:
     *  ActionPort: '<S156>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S157>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem1' */
  }

  /* End of If: '<S4>/If' */

  /* DiscreteIntegrator: '<S89>/Integrator' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState <= 0))
  {
    ControlePosicao_DW.Integrator_DSTATE = 0.0;
  }

  /* DiscreteIntegrator: '<S84>/Filter' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Filter_PrevResetState <= 0)) {
    ControlePosicao_DW.Filter_DSTATE = 0.0;
  }

  /* Gain: '<S92>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S84>/Filter'
   *  Gain: '<S83>/Derivative Gain'
   *  Sum: '<S84>/SumD'
   */
  rtb_FilterCoefficient = (0.102966199285408 * rtb_Integrator_p -
    ControlePosicao_DW.Filter_DSTATE) * 1.22722477558936;

  /* Sum: '<S98>/Sum' incorporates:
   *  DiscreteIntegrator: '<S89>/Integrator'
   *  Gain: '<S94>/Proportional Gain'
   */
  rtb_Integrator_d = (1.80266580450494 * rtb_Integrator_p +
                      ControlePosicao_DW.Integrator_DSTATE) +
    rtb_FilterCoefficient;

  /* Saturate: '<S96>/Saturation' incorporates:
   *  DeadZone: '<S82>/DeadZone'
   */
  if (rtb_Integrator_d > 3.0) {
    /* Saturate: '<S96>/Saturation' */
    ControlePosicao_Y.SinalControleX = 3.0;
    rtb_Integrator_d -= 3.0;
  } else {
    if (rtb_Integrator_d < -3.0) {
      /* Saturate: '<S96>/Saturation' */
      ControlePosicao_Y.SinalControleX = -3.0;
    } else {
      /* Saturate: '<S96>/Saturation' */
      ControlePosicao_Y.SinalControleX = rtb_Integrator_d;
    }

    if (rtb_Integrator_d >= -3.0) {
      rtb_Integrator_d = 0.0;
    } else {
      rtb_Integrator_d -= -3.0;
    }
  }

  /* End of Saturate: '<S96>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  denAccum = (ControlePosicao_Y.SinalControleX - -1.97003408267869 *
              ControlePosicao_DW.TFPosicaoX_states[0]) - 0.970034082246672 *
    ControlePosicao_DW.TFPosicaoX_states[1];

  /* Outport: '<Root>/PosRoboX' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoX'
   */
  ControlePosicao_Y.PosRoboX = 0.000349407810544228 * denAccum;

  /* Outport: '<Root>/ErroX' */
  ControlePosicao_Y.ErroX = rtb_Integrator_p;

  /* RelationalOperator: '<S80>/NotEqual' */
  rtb_NotEqual = (0.0 != rtb_Integrator_d);

  /* Signum: '<S80>/SignPreSat' */
  if (rtb_Integrator_d < 0.0) {
    rtb_Integrator_d = -1.0;
  } else {
    if (rtb_Integrator_d > 0.0) {
      rtb_Integrator_d = 1.0;
    }
  }

  /* End of Signum: '<S80>/SignPreSat' */

  /* Gain: '<S86>/Integral Gain' */
  rtb_Integrator_p *= 0.0195239502119297;

  /* Signum: '<S80>/SignPreIntegrator' */
  if (rtb_Integrator_p < 0.0) {
    rtb_IntegralGain = -1.0;
  } else if (rtb_Integrator_p > 0.0) {
    rtb_IntegralGain = 1.0;
  } else {
    rtb_IntegralGain = rtb_Integrator_p;
  }

  /* End of Signum: '<S80>/SignPreIntegrator' */

  /* RelationalOperator: '<S80>/Equal1' incorporates:
   *  DataTypeConversion: '<S80>/DataTypeConv1'
   *  DataTypeConversion: '<S80>/DataTypeConv2'
   */
  rtb_Equal1 = (static_cast<int8_T>(rtb_Integrator_d) == static_cast<int8_T>
                (rtb_IntegralGain));

  /* Sum: '<Root>/Sum3' incorporates:
   *  Inport: '<Root>/PosY'
   *  Inport: '<Root>/ReAlimY'
   */
  rtb_Integrator_d = ControlePosicao_U.PosY - ControlePosicao_U.ReAlimY;

  /* If: '<S5>/If' incorporates:
   *  Constant: '<S5>/Constant'
   *  Constant: '<S5>/Constant1'
   */
  if (rtb_Integrator_d <= 0.001) {
    /* Outputs for IfAction SubSystem: '<S5>/If Action Subsystem' incorporates:
     *  ActionPort: '<S158>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge_j);

    /* End of Outputs for SubSystem: '<S5>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S5>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S159>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge_j);

    /* End of Outputs for SubSystem: '<S5>/If Action Subsystem1' */
  }

  /* End of If: '<S5>/If' */

  /* DiscreteIntegrator: '<S139>/Integrator' */
  if ((rtb_Merge_j > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState_p <=
       0)) {
    ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  }

  /* DiscreteIntegrator: '<S134>/Filter' */
  if ((rtb_Merge_j > 0.0) && (ControlePosicao_DW.Filter_PrevResetState_o <= 0))
  {
    ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  }

  /* Gain: '<S142>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S134>/Filter'
   *  Gain: '<S133>/Derivative Gain'
   *  Sum: '<S134>/SumD'
   */
  rtb_FilterCoefficient_l = (0.0252510897395415 * rtb_Integrator_d -
    ControlePosicao_DW.Filter_DSTATE_b) * 1.70599272259623;

  /* Sum: '<S148>/Sum' incorporates:
   *  DiscreteIntegrator: '<S139>/Integrator'
   *  Gain: '<S144>/Proportional Gain'
   */
  rtb_IntegralGain = (2.62167273219043 * rtb_Integrator_d +
                      ControlePosicao_DW.Integrator_DSTATE_i) +
    rtb_FilterCoefficient_l;

  /* Saturate: '<S146>/Saturation' incorporates:
   *  DeadZone: '<S132>/DeadZone'
   */
  if (rtb_IntegralGain > 3.0) {
    /* Saturate: '<S146>/Saturation' */
    ControlePosicao_Y.SinalControleY = 3.0;
    rtb_IntegralGain -= 3.0;
  } else {
    if (rtb_IntegralGain < -3.0) {
      /* Saturate: '<S146>/Saturation' */
      ControlePosicao_Y.SinalControleY = -3.0;
    } else {
      /* Saturate: '<S146>/Saturation' */
      ControlePosicao_Y.SinalControleY = rtb_IntegralGain;
    }

    if (rtb_IntegralGain >= -3.0) {
      rtb_IntegralGain = 0.0;
    } else {
      rtb_IntegralGain -= -3.0;
    }
  }

  /* End of Saturate: '<S146>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  denAccum_0 = (ControlePosicao_Y.SinalControleY - -1.93989434819277 *
                ControlePosicao_DW.TFPosicaoY_states[0]) - 0.939900946935122 *
    ControlePosicao_DW.TFPosicaoY_states[1];

  /* Outport: '<Root>/ErroY' */
  ControlePosicao_Y.ErroY = rtb_Integrator_d;

  /* RelationalOperator: '<S130>/NotEqual' */
  rtb_NotEqual_o = (0.0 != rtb_IntegralGain);

  /* Signum: '<S130>/SignPreSat' */
  if (rtb_IntegralGain < 0.0) {
    rtb_IntegralGain = -1.0;
  } else {
    if (rtb_IntegralGain > 0.0) {
      rtb_IntegralGain = 1.0;
    }
  }

  /* End of Signum: '<S130>/SignPreSat' */

  /* Gain: '<S136>/Integral Gain' */
  rtb_Integrator_d *= 0.0396327631747005;

  /* Signum: '<S130>/SignPreIntegrator' */
  if (rtb_Integrator_d < 0.0) {
    rtb_FilterCoefficient_f = -1.0;
  } else if (rtb_Integrator_d > 0.0) {
    rtb_FilterCoefficient_f = 1.0;
  } else {
    rtb_FilterCoefficient_f = rtb_Integrator_d;
  }

  /* End of Signum: '<S130>/SignPreIntegrator' */

  /* RelationalOperator: '<S130>/Equal1' incorporates:
   *  DataTypeConversion: '<S130>/DataTypeConv1'
   *  DataTypeConversion: '<S130>/DataTypeConv2'
   */
  rtb_Equal1_lz = (static_cast<int8_T>(rtb_IntegralGain) == static_cast<int8_T>
                   (rtb_FilterCoefficient_f));

  /* Sum: '<Root>/Sum1' incorporates:
   *  Inport: '<Root>/PosAngular'
   *  Inport: '<Root>/ReAlimAngular'
   */
  rtb_IntegralGain = ControlePosicao_U.PosAngular -
    ControlePosicao_U.ReAlimAngular;

  /* Gain: '<S42>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S34>/Filter'
   *  Gain: '<S33>/Derivative Gain'
   *  Sum: '<S34>/SumD'
   */
  rtb_FilterCoefficient_f = (0.648198669917731 * rtb_IntegralGain -
    ControlePosicao_DW.Filter_DSTATE_d) * 13.765961843175;

  /* Sum: '<S48>/Sum' incorporates:
   *  DiscreteIntegrator: '<S39>/Integrator'
   *  Gain: '<S44>/Proportional Gain'
   */
  rtb_SignPreIntegrator = (6.94202553376377 * rtb_IntegralGain +
    ControlePosicao_DW.Integrator_DSTATE_b) + rtb_FilterCoefficient_f;

  /* Saturate: '<S46>/Saturation' incorporates:
   *  DeadZone: '<S32>/DeadZone'
   */
  if (rtb_SignPreIntegrator > 6.25) {
    /* Saturate: '<S46>/Saturation' */
    ControlePosicao_Y.SinalControleW = 6.25;
    rtb_SignPreIntegrator -= 6.25;
  } else {
    if (rtb_SignPreIntegrator < -6.25) {
      /* Saturate: '<S46>/Saturation' */
      ControlePosicao_Y.SinalControleW = -6.25;
    } else {
      /* Saturate: '<S46>/Saturation' */
      ControlePosicao_Y.SinalControleW = rtb_SignPreIntegrator;
    }

    if (rtb_SignPreIntegrator >= -6.25) {
      rtb_SignPreIntegrator = 0.0;
    } else {
      rtb_SignPreIntegrator -= -6.25;
    }
  }

  /* End of Saturate: '<S46>/Saturation' */

  /* DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  denAccum_1 = (ControlePosicao_Y.SinalControleW - -1.90090134081304 *
                ControlePosicao_DW.TFPosicaoAngular_states[0]) -
    0.900906322193617 * ControlePosicao_DW.TFPosicaoAngular_states[1];

  /* Outport: '<Root>/PosRoboY' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoY'
   */
  ControlePosicao_Y.PosRoboY = 0.000637817571138752 * denAccum_0;

  /* Outport: '<Root>/PosRoboAngular' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoAngular'
   */
  ControlePosicao_Y.PosRoboAngular = 0.00162307861372471 * denAccum_1;

  /* Outport: '<Root>/ErroW' */
  ControlePosicao_Y.ErroW = rtb_IntegralGain;

  /* RelationalOperator: '<S30>/NotEqual' */
  rtb_NotEqual_p2 = (0.0 != rtb_SignPreIntegrator);

  /* Signum: '<S30>/SignPreSat' */
  if (rtb_SignPreIntegrator < 0.0) {
    rtb_SignPreIntegrator = -1.0;
  } else {
    if (rtb_SignPreIntegrator > 0.0) {
      rtb_SignPreIntegrator = 1.0;
    }
  }

  /* End of Signum: '<S30>/SignPreSat' */

  /* Gain: '<S36>/Integral Gain' */
  rtb_IntegralGain *= 0.436430943774027;

  /* Switch: '<S80>/Switch' incorporates:
   *  Constant: '<S80>/Constant1'
   *  Logic: '<S80>/AND3'
   */
  if (rtb_NotEqual && rtb_Equal1) {
    rtb_Integrator_p = 0.0;
  }

  /* End of Switch: '<S80>/Switch' */

  /* Update for DiscreteIntegrator: '<S89>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE += 0.015 * rtb_Integrator_p;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S89>/Integrator' */

  /* Update for DiscreteIntegrator: '<S84>/Filter' */
  ControlePosicao_DW.Filter_DSTATE += 0.015 * rtb_FilterCoefficient;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S84>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[1] =
    ControlePosicao_DW.TFPosicaoX_states[0];
  ControlePosicao_DW.TFPosicaoX_states[0] = denAccum;

  /* Switch: '<S130>/Switch' incorporates:
   *  Constant: '<S130>/Constant1'
   *  Logic: '<S130>/AND3'
   */
  if (rtb_NotEqual_o && rtb_Equal1_lz) {
    rtb_Integrator_d = 0.0;
  }

  /* End of Switch: '<S130>/Switch' */

  /* Update for DiscreteIntegrator: '<S139>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_i += 0.015 * rtb_Integrator_d;
  if (rtb_Merge_j > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 1;
  } else if (rtb_Merge_j < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = -1;
  } else if (rtb_Merge_j == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState_p = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S139>/Integrator' */

  /* Update for DiscreteIntegrator: '<S134>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b += 0.015 * rtb_FilterCoefficient_l;
  if (rtb_Merge_j > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = 1;
  } else if (rtb_Merge_j < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = -1;
  } else if (rtb_Merge_j == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState_o = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S134>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[1] =
    ControlePosicao_DW.TFPosicaoY_states[0];
  ControlePosicao_DW.TFPosicaoY_states[0] = denAccum_0;

  /* Signum: '<S30>/SignPreIntegrator' */
  if (rtb_IntegralGain < 0.0) {
    rtb_Integrator_p = -1.0;
  } else if (rtb_IntegralGain > 0.0) {
    rtb_Integrator_p = 1.0;
  } else {
    rtb_Integrator_p = rtb_IntegralGain;
  }

  /* End of Signum: '<S30>/SignPreIntegrator' */

  /* Switch: '<S30>/Switch' incorporates:
   *  Constant: '<S30>/Constant1'
   *  DataTypeConversion: '<S30>/DataTypeConv1'
   *  DataTypeConversion: '<S30>/DataTypeConv2'
   *  Logic: '<S30>/AND3'
   *  RelationalOperator: '<S30>/Equal1'
   */
  if (rtb_NotEqual_p2 && (static_cast<int8_T>(rtb_SignPreIntegrator) ==
                          static_cast<int8_T>(rtb_Integrator_p))) {
    rtb_IntegralGain = 0.0;
  }

  /* End of Switch: '<S30>/Switch' */

  /* Update for DiscreteIntegrator: '<S39>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_b += 0.015 * rtb_IntegralGain;

  /* Update for DiscreteIntegrator: '<S34>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_d += 0.015 * rtb_FilterCoefficient_f;

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  ControlePosicao_DW.TFPosicaoAngular_states[1] =
    ControlePosicao_DW.TFPosicaoAngular_states[0];
  ControlePosicao_DW.TFPosicaoAngular_states[0] = denAccum_1;
}

/* Model initialize function */
void ControlePosicaoModelClass::initialize()
{
  /* InitializeConditions for DiscreteIntegrator: '<S89>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S84>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S139>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState_p = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S134>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState_o = 2;
}

/* Model terminate function */
void ControlePosicaoModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControlePosicaoModelClass::ControlePosicaoModelClass() :
  ControlePosicao_DW(),
  ControlePosicao_U(),
  ControlePosicao_Y(),
  ControlePosicao_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControlePosicaoModelClass::~ControlePosicaoModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControlePosicao_T * ControlePosicaoModelClass::getRTM()
{
  return (&ControlePosicao_M);
}
