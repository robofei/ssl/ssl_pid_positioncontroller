clear; close all; clc;
%% Adiciona as funções auxiliares
addpath('importFunctions/');

%% Recebe os dados do experimento
Endereco = uigetdir(pwd, "Selecione a pasta que possui os dados");
if length(Endereco) < 2
    return;
end

EndX = strcat(Endereco, "/Data_X.csv");
EndY = strcat(Endereco, "/Data_Y.csv");
EndW = strcat(Endereco, "/Data_W.csv");
%% Carrega os dados do experimento
[  ~   , VelX, ~ , PosXX,   ~  ] = importfileXY(EndX, [2, Inf]);
[  ~   , VelY, ~ ,   ~  , PosYY] = importfileXY(EndY, [2, Inf]);
[  ~   , VelW, ~ , Angle]        =  importfileW(EndW, [2, Inf]);

clear EndX EndY EndW Endereco;
%% Cria as estruturas de dados para usar na identificação dos modelos
dadosRobo = iddata([PosXX,PosYY,Angle],[VelX,VelY,VelW],15e-3);
dadosRobo.OutputName = {'Posição X', 'Posição Y', 'Angulo'};
dadosRobo.OutputUnit = {'m','m','rad'};
dadosRobo.InputName  = {'VX','VY','VW'};
dadosRobo.InputUnit  = {'m/s','m/s','rad/s'};

clear VelX VelY VelW VelXMed VelYMed VelWMed PosXX PosYY Angle Tempo;
%% Identifica o modelo em espaço de estados
Options = n4sidOptions;       

% Estima um modelo em SS de terceira ordem
modeloSS = n4sid(dadosRobo, 3, Options); % modelo discreto
modeloSSCont = n4sid(dadosRobo, 3, 'Ts', 0, Options); % modelo continuo

pred = inputdlg('',...
                'Insira o limite de predição para comparação', [1 50]);

if isempty(pred)
    pred = {'1'};
end

compare(dadosRobo, modeloSS, str2num(pred{1})); %#ok<ST2NM>
grid on;
%% Exporta o modelo para o simulink
exportar = questdlg('Exportar o modelo para o simulink?', ...
	'Exportar Modelo', ...
	"Sim","Nao","Sim");

if exportar == "Sim"
    ControleMultiVariavel;
    prompt = {'Nome do modelo do simulink:',...
              'Nome do modelo SS discreto:',...
              'Nome do modelo SS continuo'};
    dlgtitle = 'Dados do modelo no simulink';
    dims = [1 50];
    definput = {'ControleMultiVariavel','ssModel','ssContinuo'};
    
    nomes = inputdlg(prompt, dlgtitle, dims, definput); 
    
    if ~isempty(nomes)
        set_param(strcat(nomes{1}, '/', nomes{2}), ...
                  'A', 'modeloSS.A', 'B', 'modeloSS.B', ...
                  'C', 'modeloSS.C', 'D', 'modeloSS.D', ...
                  'SampleTime', 'modeloSS.Ts');

        set_param(strcat(nomes{1}, '/', nomes{3}), ...
                  'A', 'modeloSSCont.A', 'B', 'modeloSSCont.B', ...
                  'C', 'modeloSSCont.C', 'D', 'modeloSSCont.D');
    end
%% Checa a controlabilidade e observabilidade
Mc = [modeloSSCont.B, ...
      modeloSSCont.A*modeloSSCont.B, ...
      modeloSSCont.A^2*modeloSSCont.B];
disp("Mc =");
disp(Mc)
disp(rref(Mc));

Ma = [modeloSSCont.C; ...
      modeloSSCont.C*modeloSSCont.A; ...
      modeloSSCont.C*modeloSSCont.A^2];
disp("Ma =");
disp(Ma);
disp(rref(Ma));
          
%% Faz o projeto do controlador para o modelo continuo
    % Polos de malha fechada
%     P = 2*[-1+1*sqrt(3)*1i -1-1*sqrt(3)*1i -10];
    P = [-4.3+pi*1i -4.3-pi*1i -10];

    % Matriz de ganhos
    k = place(modeloSSCont.A, modeloSSCont.B, P);
    disp('Matriz de ganhos (k):');
    disp(k);
    
    if ~isempty(nomes)
        % Exporta os ganhos para o simulink
        set_param(strcat(nomes{1},'/-k*u'),'Gain', '-k');
    end
end
