%% Código que realiza a identificação dos modelos de Função de Transferência do SSL
clear; close all; clc;
%% Adiciona as funções auxiliares
addpath('importFunctions/');
%% Carrega os dados do experimento
aux = split(pwd, '/');
Endereco = join(aux(1:end-1), '/'); 
% Esse é a pasta padrão onde está o teste mais atual
Endereco = Endereco + "/Dados/TDP2020/Teste_Controlador";
% Endereco = uigetdir(pwd,"Select the folder containing the experiment data");
% [ Time, Veli, VeliMeasured, Posii, Posij]; i = {x,y}; j = {x,y}
[  ~  , VelX, VelXMed, PosXX, PosYX] = importfileXY(Endereco+"/Data_"...
                                                       +"X.csv", [2, Inf]);
[  ~  , VelY, VelYMed, PosXY, PosYY] = importfileXY(Endereco+"/Data_"...
                                                       +"Y.csv", [2, Inf]);
% [ Time, VelW, VelWMeasured, Angle]
[Tempo, VelW, VelWMed, Angle]        = importfileW(Endereco+"/Data_"...
                                                       +"W.csv", [2, Inf]);

%% Cria as estruturas de dados para usar na identificação dos modelos
% Normalização da entrada para partir do zero
PosicaoInicialX = PosXX(1);
PosicaoInicialY = PosYY(1);
AnguloInicial   = Angle(1);

% Normalização da saída para partir do zero
DeslocamentoSaidaX   = PosXX - PosicaoInicialX;
DeslocamentoSaidaY   = PosYY - PosicaoInicialY;
DeslocamentoSaidaW   = Angle - AnguloInicial;

Tsample = 0.015;

XRobo = iddata(DeslocamentoSaidaX,VelX,Tsample,'InputName','Vx', ...
                                            'OutputName','RobotPosX',...
                                            'InputUnit','m/s',...
                                            'OutputUnit','m');
YRobo = iddata(DeslocamentoSaidaY,VelY,Tsample,'InputName','Vy',...
                                            'OutputName','RobotPosY',...
                                            'InputUnit','m/s',...
                                            'OutputUnit','m');
WRobo = iddata(DeslocamentoSaidaW,VelW,Tsample,'InputName','Vw',...
                                            'OutputName','RobotAngle',...
                                            'InputUnit','rad/s',...
                                            'OutputUnit','rad');
                          
%% Mostra os gráficos dos dados carregados
subplot(2,3,1);
plot(XRobo);
grid on;
subplot(2,3,2);
plot(YRobo);
grid on;
subplot(2,3,3);
plot(WRobo);
grid on;

%% Cria os modelos 
% Transfer function estimation options
Options = tfestOptions;
Options.Display = 'off';
Options.WeightingFilter = [];

 % 2 polos e 0 zeros
tfPosicaoXRobo = tfest(XRobo, 2, 0, Options, 'Ts', Tsample)
tfPosicaoYRobo = tfest(YRobo, 2, 0, Options, 'Ts', Tsample)
tfPosicaoWRobo = tfest(WRobo, 2, 0, Options, 'Ts', Tsample)

%% Mostra as FTs e troca a variável de z^-1 para z

tfRobotX     = tf([tfPosicaoXRobo.Numerator 0 0], tfPosicaoXRobo.Denominator,...
                  Tsample,'Variable','z')
tfRobotY     = tf([tfPosicaoYRobo.Numerator 0 0], tfPosicaoYRobo.Denominator,...
                  Tsample,'Variable','z')
tfRobotTheta = tf([tfPosicaoWRobo.Numerator 0 0], tfPosicaoWRobo.Denominator,...
                  Tsample,'Variable','z')

%% Compara os modelos
subplot(2,3,4);
compare(XRobo(1:length(PosXX)), tfRobotX);
grid on;

subplot(2,3,5);
compare(YRobo(1:length(PosYY)), tfRobotY);
grid on;

subplot(2,3,6);
compare(WRobo(1:length(Angle)), tfRobotTheta);
grid on;
