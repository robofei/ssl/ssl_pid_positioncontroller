%% Gera os modelos ARX e ARMAX para o robô do SSL
clear; close all; clc;
%% Adiciona as funções auxiliares
addpath('importFunctions/');
addpath('evaluationFunctions/');
addpath('auxFunctions/');
%% Carrega os dados do experimento
Endereco = uigetdir(pwd,"Select the folder containing the experiment data");
[robotData, dataLength] = getIDData(Endereco + "/");
plot(robotData)

%% Cria os modelos 
OptArx = arxOptions;
OptArx.Focus = 'prediction';
OptArmax = armaxOptions;
OptArmax.Focus = 'prediction';

% Essas ordens (6;5;6 e 3;2;2;5) foram os melhores valores obtidos para o
% nosso robô
% Ordens do modelo ARX
NA = 6*ones(3,3);
NB = 5*ones(3,3);
NK = 6*ones(3,3);

% Ordens do modelo ARMAX
NAm = 3*ones(3,3);
NBm = 2*ones(3,3);
NC  = 2*ones(3,1);
NKm = 5*ones(3,3);

% Estima os modelos utilizando metade dos dados
armaxModel = armax(robotData(1:dataLength/2), [NAm NBm NC NKm], OptArmax);
arxModel = arx(robotData(1:dataLength/2), [NA NB NK]);

%% Testa os modelos
meio = dataLength;
if mod(meio,2) ~= 0
    meio = meio -1;
    meio = meio/2;
else
    meio = meio/2;
end

% Horizonte de predições utilizado na comparação
pred = [1,5,10,20];
for n=1:1:4
    grafico = figure;
    grafico.WindowState = 'maximized';
    compare(robotData(meio:dataLength), arxModel, armaxModel, pred(n));
    grid on;
end

%% Calculo do coeficiente de correlacao entre as variaveis de entrada
%Para um sistema MIMO nao pode haver correlacao entre as entradas
disp('Coeficiente de correlação entre os sinais de entrada');
corrcoef(robotData.u(:,1), robotData.u(:,2))
corrcoef(robotData.u(:,1), robotData.u(:,3))
corrcoef(robotData.u(:,2), robotData.u(:,3))

%% Calculo da funcao de auto-covariancia dos sinais amostrados
%Serve para determinar se houve super-amostragem dos ssdados
figure;
[c,lags] = xcorr(robotData.y(:,1), 'normalized');
plot(lags,c);
title('Função de auto-covariância da posição X');
figure;
[c,lags] = xcorr(robotData.y(:,2), 'normalized');
plot(lags,c);
title('Função de auto-covariância da posição Y');
figure;
[c,lags] = xcorr(robotData.y(:,3), 'normalized');
plot(lags,c);
title('Função de auto-covariância da posição angular');

