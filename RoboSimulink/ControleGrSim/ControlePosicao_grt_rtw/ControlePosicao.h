/*
 * ControlePosicao.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.120
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C++ source code generated on : Fri Nov 13 23:45:21 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#ifndef RTW_HEADER_ControlePosicao_h_
#define RTW_HEADER_ControlePosicao_h_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "ControlePosicao_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S36>/Integrator' */
  real_T Filter_DSTATE;                /* '<S31>/Filter' */
  real_T TFPosicaoX_states[2];         /* '<Root>/TFPosicaoX' */
  real_T Integrator_DSTATE_i;          /* '<S84>/Integrator' */
  real_T Filter_DSTATE_b;              /* '<S79>/Filter' */
  real_T TFPosicaoY_states[2];         /* '<Root>/TFPosicaoY' */
  real_T TFPosicaoAngular_states[2];   /* '<Root>/TFPosicaoAngular' */
  int8_T Integrator_PrevResetState;    /* '<S36>/Integrator' */
  int8_T Filter_PrevResetState;        /* '<S31>/Filter' */
  int8_T Integrator_PrevResetState_p;  /* '<S84>/Integrator' */
  int8_T Filter_PrevResetState_o;      /* '<S79>/Filter' */
} DW_ControlePosicao_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T PosX;                         /* '<Root>/PosX' */
  real_T PosY;                         /* '<Root>/PosY' */
  real_T ReAlimX;                      /* '<Root>/ReAlimX' */
  real_T ReAlimY;                      /* '<Root>/ReAlimY' */
  real_T PosAngular;                   /* '<Root>/PosAngular' */
  real_T ReAlimAngular;                /* '<Root>/ReAlimAngular' */
} ExtU_ControlePosicao_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T SinalControleX;               /* '<Root>/SinalControleX' */
  real_T SinalControleY;               /* '<Root>/SinalControleY' */
  real_T PosRoboX;                     /* '<Root>/PosRoboX' */
  real_T PosRoboY;                     /* '<Root>/PosRoboY' */
  real_T ErroX;                        /* '<Root>/ErroX' */
  real_T ErroY;                        /* '<Root>/ErroY' */
  real_T SinalControleW;               /* '<Root>/SinalControleW' */
  real_T PosRoboAngular;               /* '<Root>/PosRoboAngular' */
  real_T ErroW;                        /* '<Root>/ErroW' */
} ExtY_ControlePosicao_T;

/* Real-time Model Data Structure */
struct tag_RTM_ControlePosicao_T {
  const char_T *errorStatus;
};

/* Class declaration for model ControlePosicao */
class ControlePosicaoModelClass {
  /* public data and function members */
 public:
  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  ControlePosicaoModelClass();

  /* Destructor */
  ~ControlePosicaoModelClass();

  /* Root-level structure-based inputs set method */

  /* Root inports set method */
  void setExternalInputs(const ExtU_ControlePosicao_T* pExtU_ControlePosicao_T)
  {
    ControlePosicao_U = *pExtU_ControlePosicao_T;
  }

  /* Root-level structure-based outputs get method */

  /* Root outports get method */
  const ExtY_ControlePosicao_T & getExternalOutputs() const
  {
    return ControlePosicao_Y;
  }

  /* Real-Time Model get method */
  RT_MODEL_ControlePosicao_T * getRTM();

  /* private data and function members */
 private:
  /* Block states */
  DW_ControlePosicao_T ControlePosicao_DW;

  /* External inputs */
  ExtU_ControlePosicao_T ControlePosicao_U;

  /* External outputs */
  ExtY_ControlePosicao_T ControlePosicao_Y;

  /* Real-Time Model */
  RT_MODEL_ControlePosicao_T ControlePosicao_M;

  /* private member function(s) for subsystem '<S3>/If Action Subsystem'*/
  void ControlePosic_IfActionSubsystem(real_T rtu_In1, real_T *rty_Out1);
};

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Scope' : Unused code path elimination
 * Block '<Root>/Scope1' : Unused code path elimination
 * Block '<Root>/Scope2' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ControlePosicao'
 * '<S1>'   : 'ControlePosicao/ControladorX'
 * '<S2>'   : 'ControlePosicao/ControladorY'
 * '<S3>'   : 'ControlePosicao/PID_Reset'
 * '<S4>'   : 'ControlePosicao/PID_Reset1'
 * '<S5>'   : 'ControlePosicao/ControladorX/Anti-windup'
 * '<S6>'   : 'ControlePosicao/ControladorX/D Gain'
 * '<S7>'   : 'ControlePosicao/ControladorX/Filter'
 * '<S8>'   : 'ControlePosicao/ControladorX/Filter ICs'
 * '<S9>'   : 'ControlePosicao/ControladorX/I Gain'
 * '<S10>'  : 'ControlePosicao/ControladorX/Ideal P Gain'
 * '<S11>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk'
 * '<S12>'  : 'ControlePosicao/ControladorX/Integrator'
 * '<S13>'  : 'ControlePosicao/ControladorX/Integrator ICs'
 * '<S14>'  : 'ControlePosicao/ControladorX/N Copy'
 * '<S15>'  : 'ControlePosicao/ControladorX/N Gain'
 * '<S16>'  : 'ControlePosicao/ControladorX/P Copy'
 * '<S17>'  : 'ControlePosicao/ControladorX/Parallel P Gain'
 * '<S18>'  : 'ControlePosicao/ControladorX/Reset Signal'
 * '<S19>'  : 'ControlePosicao/ControladorX/Saturation'
 * '<S20>'  : 'ControlePosicao/ControladorX/Saturation Fdbk'
 * '<S21>'  : 'ControlePosicao/ControladorX/Sum'
 * '<S22>'  : 'ControlePosicao/ControladorX/Sum Fdbk'
 * '<S23>'  : 'ControlePosicao/ControladorX/Tracking Mode'
 * '<S24>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum'
 * '<S25>'  : 'ControlePosicao/ControladorX/Tsamp - Integral'
 * '<S26>'  : 'ControlePosicao/ControladorX/Tsamp - Ngain'
 * '<S27>'  : 'ControlePosicao/ControladorX/postSat Signal'
 * '<S28>'  : 'ControlePosicao/ControladorX/preSat Signal'
 * '<S29>'  : 'ControlePosicao/ControladorX/Anti-windup/Passthrough'
 * '<S30>'  : 'ControlePosicao/ControladorX/D Gain/Internal Parameters'
 * '<S31>'  : 'ControlePosicao/ControladorX/Filter/Disc. Forward Euler Filter'
 * '<S32>'  : 'ControlePosicao/ControladorX/Filter ICs/Internal IC - Filter'
 * '<S33>'  : 'ControlePosicao/ControladorX/I Gain/Internal Parameters'
 * '<S34>'  : 'ControlePosicao/ControladorX/Ideal P Gain/Passthrough'
 * '<S35>'  : 'ControlePosicao/ControladorX/Ideal P Gain Fdbk/Disabled'
 * '<S36>'  : 'ControlePosicao/ControladorX/Integrator/Discrete'
 * '<S37>'  : 'ControlePosicao/ControladorX/Integrator ICs/Internal IC'
 * '<S38>'  : 'ControlePosicao/ControladorX/N Copy/Disabled'
 * '<S39>'  : 'ControlePosicao/ControladorX/N Gain/Internal Parameters'
 * '<S40>'  : 'ControlePosicao/ControladorX/P Copy/Disabled'
 * '<S41>'  : 'ControlePosicao/ControladorX/Parallel P Gain/Internal Parameters'
 * '<S42>'  : 'ControlePosicao/ControladorX/Reset Signal/External Reset'
 * '<S43>'  : 'ControlePosicao/ControladorX/Saturation/Passthrough'
 * '<S44>'  : 'ControlePosicao/ControladorX/Saturation Fdbk/Disabled'
 * '<S45>'  : 'ControlePosicao/ControladorX/Sum/Sum_PID'
 * '<S46>'  : 'ControlePosicao/ControladorX/Sum Fdbk/Disabled'
 * '<S47>'  : 'ControlePosicao/ControladorX/Tracking Mode/Disabled'
 * '<S48>'  : 'ControlePosicao/ControladorX/Tracking Mode Sum/Passthrough'
 * '<S49>'  : 'ControlePosicao/ControladorX/Tsamp - Integral/Passthrough'
 * '<S50>'  : 'ControlePosicao/ControladorX/Tsamp - Ngain/Passthrough'
 * '<S51>'  : 'ControlePosicao/ControladorX/postSat Signal/Forward_Path'
 * '<S52>'  : 'ControlePosicao/ControladorX/preSat Signal/Forward_Path'
 * '<S53>'  : 'ControlePosicao/ControladorY/Anti-windup'
 * '<S54>'  : 'ControlePosicao/ControladorY/D Gain'
 * '<S55>'  : 'ControlePosicao/ControladorY/Filter'
 * '<S56>'  : 'ControlePosicao/ControladorY/Filter ICs'
 * '<S57>'  : 'ControlePosicao/ControladorY/I Gain'
 * '<S58>'  : 'ControlePosicao/ControladorY/Ideal P Gain'
 * '<S59>'  : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk'
 * '<S60>'  : 'ControlePosicao/ControladorY/Integrator'
 * '<S61>'  : 'ControlePosicao/ControladorY/Integrator ICs'
 * '<S62>'  : 'ControlePosicao/ControladorY/N Copy'
 * '<S63>'  : 'ControlePosicao/ControladorY/N Gain'
 * '<S64>'  : 'ControlePosicao/ControladorY/P Copy'
 * '<S65>'  : 'ControlePosicao/ControladorY/Parallel P Gain'
 * '<S66>'  : 'ControlePosicao/ControladorY/Reset Signal'
 * '<S67>'  : 'ControlePosicao/ControladorY/Saturation'
 * '<S68>'  : 'ControlePosicao/ControladorY/Saturation Fdbk'
 * '<S69>'  : 'ControlePosicao/ControladorY/Sum'
 * '<S70>'  : 'ControlePosicao/ControladorY/Sum Fdbk'
 * '<S71>'  : 'ControlePosicao/ControladorY/Tracking Mode'
 * '<S72>'  : 'ControlePosicao/ControladorY/Tracking Mode Sum'
 * '<S73>'  : 'ControlePosicao/ControladorY/Tsamp - Integral'
 * '<S74>'  : 'ControlePosicao/ControladorY/Tsamp - Ngain'
 * '<S75>'  : 'ControlePosicao/ControladorY/postSat Signal'
 * '<S76>'  : 'ControlePosicao/ControladorY/preSat Signal'
 * '<S77>'  : 'ControlePosicao/ControladorY/Anti-windup/Passthrough'
 * '<S78>'  : 'ControlePosicao/ControladorY/D Gain/Internal Parameters'
 * '<S79>'  : 'ControlePosicao/ControladorY/Filter/Disc. Forward Euler Filter'
 * '<S80>'  : 'ControlePosicao/ControladorY/Filter ICs/Internal IC - Filter'
 * '<S81>'  : 'ControlePosicao/ControladorY/I Gain/Internal Parameters'
 * '<S82>'  : 'ControlePosicao/ControladorY/Ideal P Gain/Passthrough'
 * '<S83>'  : 'ControlePosicao/ControladorY/Ideal P Gain Fdbk/Disabled'
 * '<S84>'  : 'ControlePosicao/ControladorY/Integrator/Discrete'
 * '<S85>'  : 'ControlePosicao/ControladorY/Integrator ICs/Internal IC'
 * '<S86>'  : 'ControlePosicao/ControladorY/N Copy/Disabled'
 * '<S87>'  : 'ControlePosicao/ControladorY/N Gain/Internal Parameters'
 * '<S88>'  : 'ControlePosicao/ControladorY/P Copy/Disabled'
 * '<S89>'  : 'ControlePosicao/ControladorY/Parallel P Gain/Internal Parameters'
 * '<S90>'  : 'ControlePosicao/ControladorY/Reset Signal/External Reset'
 * '<S91>'  : 'ControlePosicao/ControladorY/Saturation/Passthrough'
 * '<S92>'  : 'ControlePosicao/ControladorY/Saturation Fdbk/Disabled'
 * '<S93>'  : 'ControlePosicao/ControladorY/Sum/Sum_PID'
 * '<S94>'  : 'ControlePosicao/ControladorY/Sum Fdbk/Disabled'
 * '<S95>'  : 'ControlePosicao/ControladorY/Tracking Mode/Disabled'
 * '<S96>'  : 'ControlePosicao/ControladorY/Tracking Mode Sum/Passthrough'
 * '<S97>'  : 'ControlePosicao/ControladorY/Tsamp - Integral/Passthrough'
 * '<S98>'  : 'ControlePosicao/ControladorY/Tsamp - Ngain/Passthrough'
 * '<S99>'  : 'ControlePosicao/ControladorY/postSat Signal/Forward_Path'
 * '<S100>' : 'ControlePosicao/ControladorY/preSat Signal/Forward_Path'
 * '<S101>' : 'ControlePosicao/PID_Reset/If Action Subsystem'
 * '<S102>' : 'ControlePosicao/PID_Reset/If Action Subsystem1'
 * '<S103>' : 'ControlePosicao/PID_Reset1/If Action Subsystem'
 * '<S104>' : 'ControlePosicao/PID_Reset1/If Action Subsystem1'
 */
#endif                                 /* RTW_HEADER_ControlePosicao_h_ */
