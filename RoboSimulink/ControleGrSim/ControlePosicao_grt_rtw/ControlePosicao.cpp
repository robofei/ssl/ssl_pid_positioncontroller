/*
 * ControlePosicao.cpp
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "ControlePosicao".
 *
 * Model version              : 1.120
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C++ source code generated on : Fri Nov 13 23:45:21 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objective: Execution efficiency
 * Validation result: All passed
 */

#include "ControlePosicao.h"
#include "ControlePosicao_private.h"

/*
 * Output and update for action system:
 *    '<S3>/If Action Subsystem'
 *    '<S3>/If Action Subsystem1'
 *    '<S4>/If Action Subsystem'
 *    '<S4>/If Action Subsystem1'
 */
void ControlePosicaoModelClass::ControlePosic_IfActionSubsystem(real_T rtu_In1,
  real_T *rty_Out1)
{
  /* Inport: '<S101>/In1' */
  *rty_Out1 = rtu_In1;
}

/* Model step function */
void ControlePosicaoModelClass::step()
{
  real_T denAccum;
  real_T denAccum_0;
  real_T denAccum_1;
  real_T rtb_Merge;
  real_T rtb_FilterCoefficient;
  real_T rtb_IntegralGain;
  real_T rtb_Merge_j;
  real_T rtb_FilterCoefficient_l;
  real_T rtb_IntegralGain_p;

  /* Sum: '<Root>/Sum2' incorporates:
   *  Inport: '<Root>/PosX'
   *  Inport: '<Root>/ReAlimX'
   */
  ControlePosicao_Y.ErroW = ControlePosicao_U.PosX - ControlePosicao_U.ReAlimX;

  /* If: '<S3>/If' incorporates:
   *  Constant: '<S3>/Constant'
   *  Constant: '<S3>/Constant1'
   */
  if (ControlePosicao_Y.ErroW <= 0.001) {
    /* Outputs for IfAction SubSystem: '<S3>/If Action Subsystem' incorporates:
     *  ActionPort: '<S101>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S3>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S3>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S102>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge);

    /* End of Outputs for SubSystem: '<S3>/If Action Subsystem1' */
  }

  /* End of If: '<S3>/If' */

  /* DiscreteIntegrator: '<S36>/Integrator' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState <= 0))
  {
    ControlePosicao_DW.Integrator_DSTATE = 0.0;
  }

  /* DiscreteIntegrator: '<S31>/Filter' */
  if ((rtb_Merge > 0.0) && (ControlePosicao_DW.Filter_PrevResetState <= 0)) {
    ControlePosicao_DW.Filter_DSTATE = 0.0;
  }

  /* Gain: '<S39>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S31>/Filter'
   *  Gain: '<S30>/Derivative Gain'
   *  Sum: '<S31>/SumD'
   */
  rtb_FilterCoefficient = (0.0256927842513253 * ControlePosicao_Y.ErroW -
    ControlePosicao_DW.Filter_DSTATE) * 95.7177509486115;

  /* Sum: '<S45>/Sum' incorporates:
   *  DiscreteIntegrator: '<S36>/Integrator'
   *  Gain: '<S41>/Proportional Gain'
   */
  ControlePosicao_Y.SinalControleX = (3.17879898542878 * ControlePosicao_Y.ErroW
    + ControlePosicao_DW.Integrator_DSTATE) + rtb_FilterCoefficient;

  /* DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  denAccum = (ControlePosicao_Y.SinalControleX - -1.901914969624509 *
              ControlePosicao_DW.TFPosicaoX_states[0]) - 0.901926316534888 *
    ControlePosicao_DW.TFPosicaoX_states[1];

  /* Outport: '<Root>/PosRoboX' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoX'
   */
  ControlePosicao_Y.PosRoboX = 0.00149419141693635 * denAccum;

  /* Outport: '<Root>/ErroX' */
  ControlePosicao_Y.ErroX = ControlePosicao_Y.ErroW;

  /* Gain: '<S33>/Integral Gain' */
  rtb_IntegralGain = 0.0825446557632767 * ControlePosicao_Y.ErroW;

  /* Sum: '<Root>/Sum3' incorporates:
   *  Inport: '<Root>/PosY'
   *  Inport: '<Root>/ReAlimY'
   */
  ControlePosicao_Y.ErroW = ControlePosicao_U.PosY - ControlePosicao_U.ReAlimY;

  /* If: '<S4>/If' incorporates:
   *  Constant: '<S4>/Constant'
   *  Constant: '<S4>/Constant1'
   */
  if (ControlePosicao_Y.ErroW <= 0.001) {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem' incorporates:
     *  ActionPort: '<S103>/Action Port'
     */
    ControlePosic_IfActionSubsystem(1.0, &rtb_Merge_j);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S4>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S104>/Action Port'
     */
    ControlePosic_IfActionSubsystem(0.0, &rtb_Merge_j);

    /* End of Outputs for SubSystem: '<S4>/If Action Subsystem1' */
  }

  /* End of If: '<S4>/If' */

  /* DiscreteIntegrator: '<S84>/Integrator' */
  if ((rtb_Merge_j > 0.0) && (ControlePosicao_DW.Integrator_PrevResetState_p <=
       0)) {
    ControlePosicao_DW.Integrator_DSTATE_i = 0.0;
  }

  /* DiscreteIntegrator: '<S79>/Filter' */
  if ((rtb_Merge_j > 0.0) && (ControlePosicao_DW.Filter_PrevResetState_o <= 0))
  {
    ControlePosicao_DW.Filter_DSTATE_b = 0.0;
  }

  /* Gain: '<S87>/Filter Coefficient' incorporates:
   *  DiscreteIntegrator: '<S79>/Filter'
   *  Gain: '<S78>/Derivative Gain'
   *  Sum: '<S79>/SumD'
   */
  rtb_FilterCoefficient_l = (0.198759798491154 * ControlePosicao_Y.ErroW -
    ControlePosicao_DW.Filter_DSTATE_b) * 3.05839338157519;

  /* Sum: '<S93>/Sum' incorporates:
   *  DiscreteIntegrator: '<S84>/Integrator'
   *  Gain: '<S89>/Proportional Gain'
   */
  ControlePosicao_Y.SinalControleY = (2.76866493955774 * ControlePosicao_Y.ErroW
    + ControlePosicao_DW.Integrator_DSTATE_i) + rtb_FilterCoefficient_l;

  /* DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  denAccum_0 = (ControlePosicao_Y.SinalControleY - -1.9405559591793831 *
                ControlePosicao_DW.TFPosicaoY_states[0]) - 0.94055595829425 *
    ControlePosicao_DW.TFPosicaoY_states[1];

  /* Outport: '<Root>/ErroY' */
  ControlePosicao_Y.ErroY = ControlePosicao_Y.ErroW;

  /* Gain: '<S81>/Integral Gain' */
  rtb_IntegralGain_p = 0.0636403734411895 * ControlePosicao_Y.ErroW;

  /* Sum: '<Root>/Sum1' incorporates:
   *  Inport: '<Root>/PosAngular'
   *  Inport: '<Root>/ReAlimAngular'
   */
  ControlePosicao_Y.ErroW = ControlePosicao_U.PosAngular -
    ControlePosicao_U.ReAlimAngular;

  /* Gain: '<Root>/Gain' */
  ControlePosicao_Y.SinalControleW = 3.0516302635667403 *
    ControlePosicao_Y.ErroW;

  /* DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  denAccum_1 = (ControlePosicao_Y.SinalControleW - -1.9151355962855059 *
                ControlePosicao_DW.TFPosicaoAngular_states[0]) -
    0.915135595351495 * ControlePosicao_DW.TFPosicaoAngular_states[1];

  /* Outport: '<Root>/PosRoboY' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoY'
   */
  ControlePosicao_Y.PosRoboY = 0.00091822947243032132 * denAccum_0;

  /* Outport: '<Root>/PosRoboAngular' incorporates:
   *  DiscreteTransferFcn: '<Root>/TFPosicaoAngular'
   */
  ControlePosicao_Y.PosRoboAngular = 0.001288661420933 * denAccum_1;

  /* Update for DiscreteIntegrator: '<S36>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE += 0.015 * rtb_IntegralGain;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S36>/Integrator' */

  /* Update for DiscreteIntegrator: '<S31>/Filter' */
  ControlePosicao_DW.Filter_DSTATE += 0.015 * rtb_FilterCoefficient;
  if (rtb_Merge > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 1;
  } else if (rtb_Merge < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = -1;
  } else if (rtb_Merge == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S31>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoX' */
  ControlePosicao_DW.TFPosicaoX_states[1] =
    ControlePosicao_DW.TFPosicaoX_states[0];
  ControlePosicao_DW.TFPosicaoX_states[0] = denAccum;

  /* Update for DiscreteIntegrator: '<S84>/Integrator' */
  ControlePosicao_DW.Integrator_DSTATE_i += 0.015 * rtb_IntegralGain_p;
  if (rtb_Merge_j > 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 1;
  } else if (rtb_Merge_j < 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = -1;
  } else if (rtb_Merge_j == 0.0) {
    ControlePosicao_DW.Integrator_PrevResetState_p = 0;
  } else {
    ControlePosicao_DW.Integrator_PrevResetState_p = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S84>/Integrator' */

  /* Update for DiscreteIntegrator: '<S79>/Filter' */
  ControlePosicao_DW.Filter_DSTATE_b += 0.015 * rtb_FilterCoefficient_l;
  if (rtb_Merge_j > 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = 1;
  } else if (rtb_Merge_j < 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = -1;
  } else if (rtb_Merge_j == 0.0) {
    ControlePosicao_DW.Filter_PrevResetState_o = 0;
  } else {
    ControlePosicao_DW.Filter_PrevResetState_o = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S79>/Filter' */

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoY' */
  ControlePosicao_DW.TFPosicaoY_states[1] =
    ControlePosicao_DW.TFPosicaoY_states[0];
  ControlePosicao_DW.TFPosicaoY_states[0] = denAccum_0;

  /* Update for DiscreteTransferFcn: '<Root>/TFPosicaoAngular' */
  ControlePosicao_DW.TFPosicaoAngular_states[1] =
    ControlePosicao_DW.TFPosicaoAngular_states[0];
  ControlePosicao_DW.TFPosicaoAngular_states[0] = denAccum_1;
}

/* Model initialize function */
void ControlePosicaoModelClass::initialize()
{
  /* InitializeConditions for DiscreteIntegrator: '<S36>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S31>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S84>/Integrator' */
  ControlePosicao_DW.Integrator_PrevResetState_p = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S79>/Filter' */
  ControlePosicao_DW.Filter_PrevResetState_o = 2;
}

/* Model terminate function */
void ControlePosicaoModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
ControlePosicaoModelClass::ControlePosicaoModelClass():
  ControlePosicao_DW()
  ,ControlePosicao_U()
  ,ControlePosicao_Y()
  ,ControlePosicao_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
ControlePosicaoModelClass::~ControlePosicaoModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_ControlePosicao_T * ControlePosicaoModelClass::getRTM()
{
  return (&ControlePosicao_M);
}
