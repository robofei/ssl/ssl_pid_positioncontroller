Esta pasta contém alguns modelos no Simulink dos robôs do SSL.

* O arquivo 'ControlePosicao.slc' é o modelo principal que é utilizado
  atualmente. Também existe uma versão para o MATLAB R2019a.

* O arquivo 'ControlePosicao_TDP_Eng.slx' é o mesmo modelo do principal só que 
com as legendas em inglês, ele foi utilizado no TDP de 2020.

* O arquivo 'ControleMultiVariavel.slx' é um modelo para teste de um
  controlador multivariável com realimentação de estados, mas não funcionou
  porque o modelo em espaço de estados do robô não está correto.
